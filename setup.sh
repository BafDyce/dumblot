#!/bin/bash

virtualenv . --python=python3
source bin/activate
pip install -r dudle/requirements.txt
pip install -r pymumble/requirements.txt
