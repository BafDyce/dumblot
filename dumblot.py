#!/usr/bin/env python3

__version__ = 'v0.1'

import argparse
import signal
import sys
import time

import dudle.dudle as dudle
import pymumble.pymumble_py3 as pymumble

class Dumblot(object):
    def __init__(self, args):
        signal.signal(signal.SIGINT, self.ctrl_handler)

        self.name = args.name or 'Dumblot'
        self.channel = args.channel
        self.comment = args.comment or 'Dumblot ({})'.format(__version__)
        self.dudle = dudle.DudleUrl(args.dudle)
        self.running = True
        self.nb_exit = 0

        self.mumble = pymumble.Mumble(args.host,
            user=self.name,
            port=args.port,
            password=args.pw,
        )
        self.mumble.callbacks.set_callback('text_received', self.msg_handler)

    def ctrl_handler(self, signal, frame):
        print("Stop signal received")
        self.stop()
        if self.nb_exit > 1:
            print("Forced Quit")
            sys.exit(0)
        self.nb_exit += 1

    def loop(self):
        while self.running and self.mumble.isAlive():
            time.sleep(.1)

    def msg_handler(self, text):
        msg = text.message.strip()
        reply = ''
        if msg == 'dudle check':
            table = dudle.DudleTable(self.dudle)
            reply = table.as_html_table()

        if text.channel_id != []:
            send_to = self.mumble.channels[text.channel_id[0]]
            send_to.send_text_message(reply)
        else:
            send_to = self.mumble.users[text.actor]
            send_to.send_message(reply)

    def run(self):
        self.mumble.start()  # start the mumble thread
        self.mumble.is_ready()  # wait for the connection
        self.set_comment()
        self.mumble.users.myself.mute()  # by sure the user is muted
        if self.channel:
            self.mumble.channels.find_by_name(self.channel).move_in()
        self.mumble.set_bandwidth(200000)

        self.loop()

    def set_comment(self):
        self.mumble.users.myself.comment(self.comment)

    def stop(self):
        self.running = False

def main():
    parser = argparse.ArgumentParser(
        description=
"""Mumble bot which displays current results of dudle poll.
See: https://dudle.inf.tu-dresden.de.
Version: {}""".format(__version__),
formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--host', required=True,
        help='Address of the mumble server')
    parser.add_argument('--port', default=64738,
        help='Port of the mumble server')
    parser.add_argument('--channel', help='Channel to connect to')
    parser.add_argument('--pw', '--password', default='',
        help='Password of the mumble server')
    parser.add_argument('--name', default=None, help='Name of the bot')
    parser.add_argument('--comment', default=None, help='Comment of the bot')
    parser.add_argument('--dudle', default='coffee', required=True,
        help='Dudle poll ID or full URL')
    parser.add_argument('--version', action='version',
                    version='%(prog)s {version}'.format(version=__version__))

    args = parser.parse_args()

    dumblot = Dumblot(args)
    dumblot.run()

if __name__ == '__main__':
    main()
