# dumblot

**du**dle + m**umbl** + b**ot**

## How to use:

**Clone repo**:

```
git clone --recursive https://git.uif-lab.eu/BafDyce/dumblot.git
```

**Install dependencies & setup**:

```
./setup.sh
```

**Run**:

```
source bin/activate
python dumblot.py --host <host> --dudle <ID or URL>
```

**Help**:

```
usage: dumblot.py [-h] --host HOST [--port PORT] [--channel CHANNEL]
                    [--pw PW] [--name NAME] [--comment COMMENT] --dudle DUDLE
                    [--version]

Mumble bot which displays current results of dudle poll.
See: https://dudle.inf.tu-dresden.de.
Version: v0.1

optional arguments:
  -h, --help            show this help message and exit
  --host HOST           Address of the mumble server
  --port PORT           Port of the mumble server
  --channel CHANNEL     Channel to connect to
  --pw PW, --password PW
                        Password of the mumble server
  --name NAME           Name of the bot
  --comment COMMENT     Comment of the bot
  --dudle DUDLE         Dudle poll ID or full URL
  --version             show program's version number and exit
```
